import logo from './logo.svg';
import './App.css';
import Matrix from './Matrix';
function App() {
  return (
    <div className='App'>
      <div className='App-header'>
        <Matrix />
      </div>
    </div>
  );
}

export default App;
