# alphabet-soup

Alphabet soup made with react

## Getting Started

Follow the steps below

### Prerequisites

The latest stable version of node

```
https://nodejs.org/es/
```

### Installing

Clone the project first
```
git clone https://gitlab.com/roldy94/alphabet-soup
```

After installing the node modolues

```
npm install
```

## Running 

To start the application

```
npm start
```

## Versioning

version 1.0

## Authors

* **Nahuel Roldan** - *Initial work* - [Software Developer](https://gitlab.com/roldy94)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details


