import React, { useState, useEffect } from 'react';
import data from './assets/db.json';

const Matrix = () => {
  const [matrix, setMatrix] = useState(data.resources[0]);
  useEffect(() => {
    patternSearch(matrix, words);
  });
  const [words, setWord] = useState('OIE');
  const [message, setMessage] = useState('');
  const [R, setRow] = useState(3);
  const [C, setColumn] = useState(3);

  // For searching in all 8 direction
  const x = [-1, -1, -1, 0, 0, 1, 1, 1];
  const y = [-1, 0, 1, -1, 1, -1, 0, 1];
  
  const handleOnChange = (e) => {
    switch (parseInt(e.target.value)) {
      case 0:
        setRow(3);
        setColumn(3);
        break;
      case 1:
        setRow(1);
        setColumn(10);
        break;
      case 2:
        setRow(5);
        setColumn(5);
        break;
      case 3:
        setRow(7);
        setColumn(2);
        break;
      default:
        break;
    }
    setMatrix(data.resources[parseInt(e.target.value)]);    
  };

  // This function searches in all
  // 8-direction from point
  // (row, col) in grid[][]
  const search2D = (grid, row, col, word) => {
    // If first character of word
    // doesn't match with
    // given starting point in grid.

    //if (grid[row][col] !== word.charAt(0)) return false;
    var len = word.length;
    // Search word in all 8 directions
    // starting from (row, col)
    for (var dir = 0; dir < 8; dir++) {
      // Initialize starting point
      // for current direction
      var k,
        rd = row + x[dir],
        cd = col + y[dir];
      // First character is already checked,
      // match remaining characters
      for (k = 1; k < len; k++) {
        // If out of bound break
        if (rd >= R || rd < 0 || cd >= C || cd < 0) break;

        // If not matched, break
        if (grid[rd][cd] !== word.charAt(k)) break;

        // Moving in particular direction
        rd += x[dir];
        cd += y[dir];
      }

      // If all character matched,
      // then value of must
      // be equal to length of word

      if (k === len) {
        return true;
      }
    }
    return false;
  };
  // Searches given word in a given
  // matrix in all 8 directions
  const patternSearch = (grid, word) => {
    // Consider every point as starting
    // point and search given word
    var count = 0;
    for (var row = 0; row < R; row++) {
      for (var col = 0; col < C; col++) {
        if (search2D(grid, row, col, word)) {
          count++;
        }
      }
    }
    if (count !== 0) setMessage(count);
  };

  const _renderMatrix = () => {
    return (
      <div>
        <div>
          <select onChange={(e) => handleOnChange(e)}>
            {data.resources.map((item, index) => {
              return (
                <option value={index}>
                  {'selecione la matrix ' + (index + 1)}
                </option>
              );
            })}
          </select>
        </div>
        <br />
        <table>
          {matrix.map((item, index) => {
            return (
              <tr>
                {item.map((value, i) => {
                  return <td>{value}</td>;
                })}
              </tr>
            );
          })}
        </table>
        <br />
        <p>{'Se han encontrado: ' + message + ' coincidencia con la palabra "OIE"'}</p>
      </div>
    );
  };
  return <div style={{ display: 'flex' }}>{_renderMatrix()}</div>;
};

export default Matrix;
